import {
    API_URL,
    API_URL_IMG,
    API_URL_API,
    API_URL_AUT,
    API_URL_LOG,
    API_URL_SLI,
    API_URL_CAR,
    API_URL_ASO,
    API_URL_PDF,
    API_URL_CED,
    API_KEY,
  } from "@env";
  
  export default {
    API_URL,
    API_URL_API,
    API_URL_SLI,
    API_URL_IMG,
    API_URL_AUT,
    API_URL_LOG,
    API_URL_CAR,
    API_URL_ASO,
    API_URL_PDF,
    API_URL_CED,
    API_KEY,
  };
  