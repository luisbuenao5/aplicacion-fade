import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
} from "react-native";
import {
  Dialog,
  Portal,
  Card,
  Title,
  Button,
  Divider,
} from "react-native-paper";
import Communications from "react-native-communications";
import config from "../../config";

class Pregrado_Vigente extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      empresas: null,
      contabilidad: null,
      finanzas: null,
      transporte: null,
      mercadotecnia: null,
      url_empresas: config.API_URL_API + "/carreras/1",
      url_contabilidad: config.API_URL_API + "/carreras/2",
      url_finanzas: config.API_URL_API + "/carreras/8",
      url_transporte: config.API_URL_API + "/carreras/12",
      url_mercadotecnia: config.API_URL_API + "/carreras/11",

      visible_empresas: false,
      visible_inf_general_emp: false,
      visible_mis_vis_emp: false,
      visible_campo_emp: false,
      visible_perfil_emp: false,

      visible_contabilidad: false,
      visible_inf_general_cont: false,
      visible_mis_vis_cont: false,
      visible_campo_cont: false,
      visible_perfil_cont: false,

      visible_finanzas: false,
      visible_inf_general_fin: false,
      visible_mis_vis_fin: false,
      visible_campo_fin: false,
      visible_perfil_fin: false,

      visible_transporte: false,
      visible_inf_general_tra: false,
      visible_mis_vis_tra: false,
      visible_campo_tra: false,
      visible_perfil_tra: false,

      visible_mercadotecnia: false,
      visible_inf_general_mer: false,
      visible_mis_vis_mer: false,
      visible_campo_mer: false,
      visible_perfil_mer: false,
    };
  }

  _showEmpresas = () => this.setState({ visible_empresas: true });
  _hideEmpresas = () => this.setState({ visible_empresas: false });
  _showInf_General_Emp = () => this.setState({ visible_inf_general_emp: true });
  _hideInf_General_Emp = () =>
    this.setState({ visible_inf_general_emp: false });
  _showMis_Vis_Emp = () => this.setState({ visible_mis_vis_emp: true });
  _hideMis_Vis_Emp = () => this.setState({ visible_mis_vis_emp: false });
  _showCampo_Emp = () => this.setState({ visible_campo_emp: true });
  _hideCampo_Emp = () => this.setState({ visible_campo_emp: false });
  _showPerfil_Emp = () => this.setState({ visible_perfil_emp: true });
  _hidePerfil_Emp = () => this.setState({ visible_perfil_emp: false });

  _showContabilidad = () => this.setState({ visible_contabilidad: true });
  _hideContabilidad = () => this.setState({ visible_contabilidad: false });
  _showInf_General_Cont = () =>
    this.setState({ visible_inf_general_cont: true });
  _hideInf_General_Cont = () =>
    this.setState({ visible_inf_general_cont: false });
  _showMis_Vis_Cont = () => this.setState({ visible_mis_vis_cont: true });
  _hideMis_Vis_Cont = () => this.setState({ visible_mis_vis_cont: false });
  _showCampo_Cont = () => this.setState({ visible_campo_cont: true });
  _hideCampo_Cont = () => this.setState({ visible_campo_cont: false });
  _showPerfil_Cont = () => this.setState({ visible_perfil_cont: true });
  _hidePerfil_Cont = () => this.setState({ visible_perfil_cont: false });

  _showFinanzas = () => this.setState({ visible_finanzas: true });
  _hideFinanzas = () => this.setState({ visible_finanzas: false });
  _showInf_General_Fin = () => this.setState({ visible_inf_general_fin: true });
  _hideInf_General_Fin = () =>
    this.setState({ visible_inf_general_fin: false });
  _showMis_Vis_Fin = () => this.setState({ visible_mis_vis_fin: true });
  _hideMis_Vis_Fin = () => this.setState({ visible_mis_vis_fin: false });
  _showCampo_Fin = () => this.setState({ visible_campo_fin: true });
  _hideCampo_Fin = () => this.setState({ visible_campo_fin: false });
  _showPerfil_Fin = () => this.setState({ visible_perfil_fin: true });
  _hidePerfil_Fin = () => this.setState({ visible_perfil_fin: false });

  _showTransporte = () => this.setState({ visible_transporte: true });
  _hideTransporte = () => this.setState({ visible_transporte: false });
  _showInf_General_Tra = () => this.setState({ visible_inf_general_tra: true });
  _hideInf_General_Tra = () =>
    this.setState({ visible_inf_general_tra: false });
  _showMis_Vis_Tra = () => this.setState({ visible_mis_vis_tra: true });
  _hideMis_Vis_Tra = () => this.setState({ visible_mis_vis_tra: false });
  _showCampo_Tra = () => this.setState({ visible_campo_tra: true });
  _hideCampo_Tra = () => this.setState({ visible_campo_tra: false });
  _showPerfil_Tra = () => this.setState({ visible_perfil_tra: true });
  _hidePerfil_Tra = () => this.setState({ visible_perfil_tra: false });

  _showMercadotecnia = () => this.setState({ visible_mercadotecnia: true });
  _hideMercadotecnia = () => this.setState({ visible_mercadotecnia: false });
  _showInf_General_Mer = () => this.setState({ visible_inf_general_mer: true });
  _hideInf_General_Mer = () =>
    this.setState({ visible_inf_general_mer: false });
  _showMis_Vis_Mer = () => this.setState({ visible_mis_vis_mer: true });
  _hideMis_Vis_Mer = () => this.setState({ visible_mis_vis_mer: false });
  _showCampo_Mer = () => this.setState({ visible_campo_mer: true });
  _hideCampo_Mer = () => this.setState({ visible_campo_mer: false });
  _showPerfil_Mer = () => this.setState({ visible_perfil_mer: true });
  _hidePerfil_Mer = () => this.setState({ visible_perfil_mer: false });

  componentDidMount = () => {
    Promise.all([
      fetch(this.state.url_empresas),
      fetch(this.state.url_contabilidad),
      fetch(this.state.url_finanzas),
      fetch(this.state.url_transporte),
      fetch(this.state.url_mercadotecnia),
    ])
      .then((values) => {
        return Promise.all(values.map((r) => r.json()));
      })
      .then(([emp, cont, fin, trans, merc]) => {
        this.setState({
          empresas: emp.datos,
          contabilidad: cont.datos,
          finanzas: fin.datos,
          transporte: trans.datos,
          mercadotecnia: merc.datos,
          loading: false,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  };

  empresas() {
    return (
      <ScrollView>
        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card
            style={{ height: 70, width: 220, backgroundColor: "#ccd4e3"  }}
            onPress={() => this._showInf_General_Emp()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Información General
              </Title>
            </Card.Content>
          </Card>
        </View>
        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card
            style={{ height: 70, width: 220,backgroundColor: "#ccd4e3" }}
            onPress={() => this._showMis_Vis_Emp()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Misión/Visión
              </Title>
            </Card.Content>
          </Card>
        </View>
        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card
            style={{ height: 70, width: 220, backgroundColor: "#ccd4e3" }}
            onPress={() => this._showCampo_Emp()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Campo Ocupacional
              </Title>
            </Card.Content>
          </Card>
        </View>
        <View
          style={{ alignSelf: "center", paddingTop: 20, paddingBottom: 20 }}
        >
          <Card
            style={{ height: 70, width: 220, backgroundColor: "#ccd4e3" }}
            onPress={() => this._showPerfil_Emp()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Perfil Profesional
              </Title>
            </Card.Content>
          </Card>
        </View>
      </ScrollView>
    );
  }

  contabilidad() {
    return (
      <ScrollView>
        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card
            style={{ height: 70, width: 220, backgroundColor: "#ccd4e3" }}
            onPress={() => this._showInf_General_Cont()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Información General
              </Title>
            </Card.Content>
          </Card>
        </View>
        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card
            style={{ height: 70, width: 220, backgroundColor: "#ccd4e3" }}
            onPress={() => this._showMis_Vis_Cont()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Misión/Visión
              </Title>
            </Card.Content>
          </Card>
        </View>
        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card
            style={{ height: 70, width: 220, backgroundColor: "#ccd4e3" }}
            onPress={() => this._showCampo_Cont()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Campo Ocupacional
              </Title>
            </Card.Content>
          </Card>
        </View>
        <View
          style={{ alignSelf: "center", paddingTop: 20, paddingBottom: 20 }}
        >
          <Card
            style={{ height: 70, width: 220, backgroundColor: "#ccd4e3" }}
            onPress={() => this._showPerfil_Cont()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Perfil Profesional
              </Title>
            </Card.Content>
          </Card>
        </View>
      </ScrollView>
    );
  }

  finanzas() {
    return (
      <ScrollView>
        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card
            style={{ height: 70, width: 220, backgroundColor: "#ccd4e3" }}
            onPress={() => this._showInf_General_Fin()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Información General
              </Title>
            </Card.Content>
          </Card>
        </View>
        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card
            style={{ height: 70, width: 220, backgroundColor: "#ccd4e3" }}
            onPress={() => this._showMis_Vis_Fin()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Misión/Visión
              </Title>
            </Card.Content>
          </Card>
        </View>
        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card
            style={{ height: 70, width: 220, backgroundColor: "#ccd4e3" }}
            onPress={() => this._showCampo_Fin()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Campo Ocupacional
              </Title>
            </Card.Content>
          </Card>
        </View>
        <View
          style={{ alignSelf: "center", paddingTop: 20, paddingBottom: 20 }}
        >
          <Card
            style={{ height: 70, width: 220, backgroundColor: "#ccd4e3" }}
            onPress={() => this._showPerfil_Fin()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Perfil Profesional
              </Title>
            </Card.Content>
          </Card>
        </View>
      </ScrollView>
    );
  }

  transporte() {
    return (
      <ScrollView>
        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card
            style={{ height: 70, width: 220, backgroundColor: "#ccd4e3" }}
            onPress={() => this._showInf_General_Tra()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Información General
              </Title>
            </Card.Content>
          </Card>
        </View>
        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card
            style={{ height: 70, width: 220 , backgroundColor: "#ccd4e3"}}
            onPress={() => this._showMis_Vis_Tra()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Misión/Visión
              </Title>
            </Card.Content>
          </Card>
        </View>
        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card
            style={{ height: 70, width: 220, backgroundColor: "#ccd4e3" }}
            onPress={() => this._showCampo_Tra()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Campo Ocupacional
              </Title>
            </Card.Content>
          </Card>
        </View>
        <View
          style={{ alignSelf: "center", paddingTop: 20, paddingBottom: 20 }}
        >
          <Card
            style={{ height: 70, width: 220, backgroundColor: "#ccd4e3" }}
            onPress={() => this._showPerfil_Tra()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Perfil Profesional
              </Title>
            </Card.Content>
          </Card>
        </View>
      </ScrollView>
    );
  }

  mercadotecnia() {
    return (
      <ScrollView>
        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card
            style={{ height: 70, width: 220, backgroundColor: "#ccd4e3" }}
            onPress={() => this._showInf_General_Mer()}
          >
            <Card.Content style={{ alignContent: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Información General
              </Title>
            </Card.Content>
          </Card>
        </View>
        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card
            style={{ height: 70, width: 220, backgroundColor: "#ccd4e3" }}
            onPress={() => this._showMis_Vis_Mer()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Misión/Visión
              </Title>
            </Card.Content>
          </Card>
        </View>
        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card
            style={{ height: 70, width: 220 , backgroundColor: "#ccd4e3"}}
            onPress={() => this._showCampo_Mer()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Campo Ocupacional
              </Title>
            </Card.Content>
          </Card>
        </View>
        <View
          style={{ alignSelf: "center", paddingTop: 20, paddingBottom: 20 }}
        >
          <Card
            style={{ height: 70, width: 220 , backgroundColor: "#ccd4e3"}}
            onPress={() => this._showPerfil_Mer()}
          >
            <Card.Content style={{ alignSelf: "center" }}>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Perfil Profesional
              </Title>
            </Card.Content>
          </Card>
        </View>
      </ScrollView>
    );
  }

  inf_general_empresas() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.subtitulo}>Modalidad de Estudio</Text>
          <Text style={styles.text}>
            {this.state.empresas.modalidad_carrera}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo}>Duración de la Carrera</Text>
          <Text style={styles.text}>
            {this.state.empresas.duracion_carrera} semestres
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo}>Título que Otorga</Text>
          <Text style={styles.text}>{this.state.empresas.titulo_carrera}</Text>
        </View>
        <View>
          <Text style={styles.subtitulo}>Malla Curricular</Text>
          <TouchableOpacity
            onPress={() =>
              Communications.web(
                config.API_URL_PDF + "/" + this.state.empresas.malla_carrera
              )
            }
          >
            <Text style={styles.text_press}>
              {this.state.empresas.malla_carrera}
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }

  inf_general_contabilidad() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.subtitulo}>Modalidad de Estudio</Text>
          <Text style={styles.text}>
            {this.state.contabilidad.modalidad_carrera}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo}>Duración de la Carrera</Text>
          <Text style={styles.text}>
            {this.state.contabilidad.duracion_carrera} semestres
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo}>Título que Otorga</Text>
          <Text style={styles.text}>
            {this.state.contabilidad.titulo_carrera}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo}>Malla Curricular</Text>
          <TouchableOpacity
            onPress={() =>
              Communications.web(
                config.API_URL_PDF + "/" + this.state.contabilidad.malla_carrera
              )
            }
          >
            <Text style={styles.text_press}>
              {this.state.contabilidad.malla_carrera}
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }

  inf_general_finanzas() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.subtitulo}>Modalidad de Estudio</Text>
          <Text style={styles.text}>
            {this.state.finanzas.modalidad_carrera}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo}>Duración de la Carrera</Text>
          <Text style={styles.text}>
            {this.state.finanzas.duracion_carrera} semestres
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo}>Título que Otorga</Text>
          <Text style={styles.text}>{this.state.finanzas.titulo_carrera}</Text>
        </View>
        <View>
          <Text style={styles.subtitulo}>Malla Curricular</Text>
          <TouchableOpacity
            onPress={() =>
              Communications.web(
                config.API_URL_PDF + "/" + this.state.finanzas.malla_carrera
              )
            }
          >
            <Text style={styles.text_press}>
              {this.state.finanzas.malla_carrera}
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }

  inf_general_transporte() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.subtitulo}>Modalidad de Estudio</Text>
          <Text style={styles.text}>
            {this.state.transporte.modalidad_carrera}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo}>Duración de la Carrera</Text>
          <Text style={styles.text}>
            {this.state.transporte.duracion_carrera}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo}>Título que Otorga</Text>
          <Text style={styles.text}>
            {this.state.transporte.titulo_carrera}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo}>Malla Curricular</Text>
          <TouchableOpacity
            onPress={() =>
              Communications.web(
                config.API_URL_PDF + "/" + this.state.transporte.malla_carrera
              )
            }
          >
            <Text style={styles.text_press}>
              {this.state.transporte.malla_carrera}
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }

  inf_general_mercadotecnia() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.subtitulo}>Modalidad de Estudio</Text>
          <Text style={styles.text}>
            {this.state.mercadotecnia.modalidad_carrera}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo}>Duración de la Carrera</Text>
          <Text style={styles.text}>
            {this.state.mercadotecnia.duracion_carrera} semestres
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo}>Título que Otorga</Text>
          <Text style={styles.text}>
            {this.state.mercadotecnia.titulo_carrera}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo}>Malla Curricular</Text>
          <TouchableOpacity
            onPress={() =>
              Communications.web(
                config.API_URL_PDF +
                  "/" +
                  this.state.mercadotecnia.malla_carrera
              )
            }
          >
            <Text style={styles.text_press}>
              {this.state.mercadotecnia.malla_carrera}
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }

  mis_vis_empresas() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Misión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.empresas.mision_carrera}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Visión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.empresas.vision_carrera}
          </Text>
        </View>
      </ScrollView>
    );
  }

  mis_vis_contabilidad() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Misión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.contabilidad.mision_carrera}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Visión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.contabilidad.vision_carrera}
          </Text>
        </View>
      </ScrollView>
    );
  }

  mis_vis_finanzas() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Misión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.finanzas.mision_carrera}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Visión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.finanzas.vision_carrera}
          </Text>
        </View>
      </ScrollView>
    );
  }

  mis_vis_transporte() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Misión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.transporte.mision_carrera}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Visión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.transporte.vision_carrera}
          </Text>
        </View>
      </ScrollView>
    );
  }

  mis_vis_mercadotecnia() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Misión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.mercadotecnia.mision_carrera}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Visión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.mercadotecnia.vision_carrera}
          </Text>
        </View>
      </ScrollView>
    );
  }

  campo_empresas() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.text_mis_vis}>
            {this.state.empresas.campo_carrera}
          </Text>
        </View>
      </ScrollView>
    );
  }

  campo_contabilidad() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.text_mis_vis}>
            {this.state.contabilidad.campo_carrera}
          </Text>
        </View>
      </ScrollView>
    );
  }

  campo_finanzas() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.text_mis_vis}>
            {this.state.finanzas.campo_carrera}
          </Text>
        </View>
      </ScrollView>
    );
  }

  campo_transporte() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.text_mis_vis}>
            {this.state.transporte.campo_carrera}
          </Text>
        </View>
      </ScrollView>
    );
  }

  campo_mercadotecnia() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.text_mis_vis}>
            {this.state.mercadotecnia.campo_carrera}
          </Text>
        </View>
      </ScrollView>
    );
  }

  perfil_empresas() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.text_mis_vis}>
            {this.state.empresas.perfil_carrera}
          </Text>
        </View>
      </ScrollView>
    );
  }

  perfil_contabilidad() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.text_mis_vis}>
            {this.state.contabilidad.perfil_carrera}
          </Text>
        </View>
      </ScrollView>
    );
  }

  perfil_finanzas() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.text_mis_vis}>
            {this.state.finanzas.perfil_carrera}
          </Text>
        </View>
      </ScrollView>
    );
  }

  perfil_transporte() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.text_mis_vis}>
            {this.state.transporte.perfil_carrera}
          </Text>
        </View>
      </ScrollView>
    );
  }

  perfil_mercadotecnia() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.text_mis_vis}>
            {this.state.mercadotecnia.perfil_carrera}
          </Text>
        </View>
      </ScrollView>
    );
  }

  render() {
    const { loading } = this.state;
    if (!loading) {
      return (
        <View style={styles.container}>
          <ImageBackground
            source={require("../../assets/images/fade-transparente.png")}
            style={{ flex: 1, flexDirection: "column" }}
          >
            <ScrollView>
              <View style={styles.politicas_calidad}>
                <Text style={styles.titulo}>Pregrado Vigente</Text>
              </View>

              <View
                style={{
                  alignSelf: "center",
                  paddingTop: 20,
                  paddingBottom: 20,
                }}
              >
                <Card
                  style={{ height: 190, width: 300 }}
                  onPress={() => this._showEmpresas()}
                >
                  <Card.Cover
                    style={{ height: 150, width: 270, alignSelf: "center" }}
                    source={{
                      uri:
                        config.API_URL_CAR +
                        "/" +
                        this.state.empresas.logo_carrera,
                    }}
                  />
                  <Card.Content style={{ alignSelf: "center" }}>
                    <Title style={{ fontSize: 17, textAlign: "center" }}>
                      {this.state.empresas.nombre_carrera}
                    </Title>
                  </Card.Content>
                </Card>
              </View>

              <Divider />

              <View
                style={{
                  alignSelf: "center",
                  paddingTop: 20,
                  paddingBottom: 20,
                }}
              >
                <Card
                  style={{ height: 220, width: 300 }}
                  onPress={() => this._showContabilidad()}
                >
                  <Card.Cover
                    style={{
                      height: 170,
                      width: 150,
                      alignSelf: "center",
                      marginTop: 10,
                    }}
                    source={{
                      uri:
                        config.API_URL_CAR +
                        "/" +
                        this.state.contabilidad.logo_carrera,
                    }}
                  />
                  <Card.Content style={{ alignSelf: "center" }}>
                    <Title style={{ fontSize: 17, textAlign: "center" }}>
                      {this.state.contabilidad.nombre_carrera}
                    </Title>
                  </Card.Content>
                </Card>
              </View>

              <Divider />

              <View
                style={{
                  alignSelf: "center",
                  paddingTop: 20,
                  paddingBottom: 20,
                }}
              >
                <Card
                  style={{ height: 250, width: 300 }}
                  onPress={() => this._showFinanzas()}
                >
                  <Card.Cover
                    style={{
                      height: 200,
                      width: 160,
                      alignSelf: "center",
                      marginTop: 10,
                    }}
                    source={{
                      uri:
                        config.API_URL_CAR +
                        "/" +
                        this.state.finanzas.logo_carrera,
                    }}
                  />
                  <Card.Content style={{ alignSelf: "center" }}>
                    <Title style={{ fontSize: 17, textAlign: "center" }}>
                      {this.state.finanzas.nombre_carrera}
                    </Title>
                  </Card.Content>
                </Card>
              </View>

              <Divider />

              <View
                style={{
                  alignSelf: "center",
                  paddingTop: 20,
                  paddingBottom: 20,
                }}
              >
                <Card
                  style={{ height: 220, width: 300 }}
                  onPress={() => this._showTransporte()}
                >
                  <Card.Cover
                    style={{
                      height: 160,
                      width: 290,
                      alignSelf: "center",
                      marginTop: 10,
                    }}
                    source={{
                      uri:
                        config.API_URL_CAR +
                        "/" +
                        this.state.transporte.logo_carrera,
                    }}
                  />
                  <Card.Content style={{ alignSelf: "center" }}>
                    <Title style={{ fontSize: 17, textAlign: "center" }}>
                      {this.state.transporte.nombre_carrera}
                    </Title>
                  </Card.Content>
                </Card>
              </View>

              <Divider />

              <View
                style={{
                  alignSelf: "center",
                  paddingTop: 20,
                  paddingBottom: 20,
                }}
              >
                <Card
                  style={{ height: 220, width: 300 }}
                  onPress={() => this._showMercadotecnia()}
                >
                  <Card.Cover
                    style={{
                      height: 160,
                      width: 290,
                      alignSelf: "center",
                      marginTop: 10,
                    }}
                    source={{
                      uri:
                        config.API_URL_CAR +
                        "/" +
                        this.state.mercadotecnia.logo_carrera,
                    }}
                  />
                  <Card.Content style={{ alignSelf: "center" }}>
                    <Title style={{ fontSize: 17, textAlign: "center" }}>
                      {this.state.mercadotecnia.nombre_carrera}
                    </Title>
                  </Card.Content>
                </Card>
              </View>

              <Divider />

              {/*--------------------------------------------------EMPRESAS---------------------------------------------------------*/}
              <Portal>
                <Dialog
                  visible={this.state.visible_empresas}
                  onDismiss={this._hideEmpresas}
                  style={{ flex: 0.6 }}
                >
                  <Dialog.ScrollArea>{this.empresas()}</Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideEmpresas}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_inf_general_emp}
                  onDismiss={this._hideInf_General_Emp}
                  style={{ flex: 0.6 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Información General
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.inf_general_empresas()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideInf_General_Emp}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_mis_vis_emp}
                  onDismiss={this._hideMis_Vis_Emp}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Misión/Visión
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.mis_vis_empresas()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideMis_Vis_Emp}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_campo_emp}
                  onDismiss={this._hideCampo_Emp}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Campo Ocupacional
                  </Dialog.Title>
                  <Dialog.ScrollArea>{this.campo_empresas()}</Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideCampo_Emp}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_perfil_emp}
                  onDismiss={this._hidePerfil_Emp}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Perfil Profesional
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.perfil_empresas()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hidePerfil_Emp}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              {/*--------------------------------------------------CONTABILIDAD------------------------------------------------------*/}
              <Portal>
                <Dialog
                  visible={this.state.visible_contabilidad}
                  onDismiss={this._hideContabilidad}
                  style={{ flex: 0.6 }}
                >
                  <Dialog.ScrollArea>{this.contabilidad()}</Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideContabilidad}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_inf_general_cont}
                  onDismiss={this._hideInf_General_Cont}
                  style={{ flex: 0.6 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Información General
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.inf_general_contabilidad()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideInf_General_Cont}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_mis_vis_cont}
                  onDismiss={this._hideMis_Vis_Cont}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Misión/Visión
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.mis_vis_contabilidad()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideMis_Vis_Cont}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_campo_cont}
                  onDismiss={this._hideCampo_Cont}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Campo Ocupacional
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.campo_contabilidad()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideCampo_Cont}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_perfil_cont}
                  onDismiss={this._hidePerfil_Cont}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Perfil Profesional
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.perfil_contabilidad()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hidePerfil_Cont}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              {/*--------------------------------------------------FINANZAS------------------------------------------------------*/}
              <Portal>
                <Dialog
                  visible={this.state.visible_finanzas}
                  onDismiss={this._hideFinanzas}
                  style={{ flex: 0.6 }}
                >
                  <Dialog.ScrollArea>{this.finanzas()}</Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideFinanzas}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_inf_general_fin}
                  onDismiss={this._hideInf_General_Fin}
                  style={{ flex: 0.6 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Información General
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.inf_general_finanzas()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideInf_General_Fin}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_mis_vis_fin}
                  onDismiss={this._hideMis_Vis_Fin}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Misión/Visión
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.mis_vis_finanzas()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideMis_Vis_Fin}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_campo_fin}
                  onDismiss={this._hideCampo_Fin}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Campo Ocupacional
                  </Dialog.Title>
                  <Dialog.ScrollArea>{this.campo_finanzas()}</Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideCampo_Fin}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_perfil_fin}
                  onDismiss={this._hidePerfil_Fin}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Perfil Profesional
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.perfil_finanzas()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hidePerfil_Fin}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              {/*--------------------------------------------------TRANSPORTE------------------------------------------------------*/}
              <Portal>
                <Dialog
                  visible={this.state.visible_transporte}
                  onDismiss={this._hideTransporte}
                  style={{ flex: 0.6 }}
                >
                  <Dialog.ScrollArea>{this.transporte()}</Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideTransporte}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_inf_general_tra}
                  onDismiss={this._hideInf_General_Tra}
                  style={{ flex: 0.6 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Información General
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.inf_general_transporte()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideInf_General_Tra}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_mis_vis_tra}
                  onDismiss={this._hideMis_Vis_Tra}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Misión/Visión
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.mis_vis_transporte()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideMis_Vis_Tra}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_campo_tra}
                  onDismiss={this._hideCampo_Tra}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Campo Ocupacional
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.campo_transporte()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideCampo_Tra}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_perfil_tra}
                  onDismiss={this._hidePerfil_Tra}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Perfil Profesional
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.perfil_transporte()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hidePerfil_Tra}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              {/*------------------------------------------------MERCADOTECNIA------------------------------------------------------*/}
              <Portal>
                <Dialog
                  visible={this.state.visible_mercadotecnia}
                  onDismiss={this._hideMercadotecnia}
                  style={{ flex: 0.6 }}
                >
                  <Dialog.ScrollArea>{this.mercadotecnia()}</Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideMercadotecnia}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_inf_general_mer}
                  onDismiss={this._hideInf_General_Mer}
                  style={{ flex: 0.6 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Información General
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.inf_general_mercadotecnia()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideInf_General_Mer}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_mis_vis_mer}
                  onDismiss={this._hideMis_Vis_Mer}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Misión/Visión
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.mis_vis_mercadotecnia()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideMis_Vis_Mer}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_campo_mer}
                  onDismiss={this._hideCampo_Mer}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Campo Ocupacional
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.campo_mercadotecnia()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideCampo_Mer}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_perfil_mer}
                  onDismiss={this._hidePerfil_Mer}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Perfil Profesional
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.perfil_mercadotecnia()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hidePerfil_Mer}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>
            </ScrollView>
          </ImageBackground>
        </View>
      );
    } else {
      return (
        <ImageBackground
          source={require("../../assets/images/fade-transparente.png")}
          style={{ flex: 1, flexDirection: "column", justifyContent: "center" }}
        >
          <ActivityIndicator
            size="large"
            color="#344a72"
            justifyContent="space-around"
          />
        </ImageBackground>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingTop: 0,
    flexDirection: "column",
  },
  text: {
    fontSize: 17,
    fontFamily: "antic-slab",
    color: "#4d4d4d",
    textAlign: "center",
    paddingTop: 14,
    paddingLeft: 5,
    paddingRight: 25,
  },
  text_press: {
    fontSize: 17,
    fontFamily: "antic-slab",
    color: "#4d4d4d",
    textAlign: "center",
    textDecorationLine: "underline",
    paddingTop: 14,
    paddingLeft: 5,
    paddingRight: 25,
  },
  text_mis_vis: {
    fontSize: 17,
    fontFamily: "antic-slab",
    color: "#4d4d4d",
    textAlign: "justify",
    paddingTop: 14,
    paddingLeft: 5,
    paddingRight: 25,
  },

  politicas_calidad: {
    flex: 1,
    marginTop: 5,
    alignSelf: "center",
  },

  titulo: {
    paddingTop: 20,
    fontWeight: "bold",
    fontSize: 30,
    color: "#344a72",
  },

  subtitulo: {
    fontSize: 17,
    fontWeight: "bold",
    color: "#4d4d4d",
    textAlign: "justify",
    paddingTop: 14,
    paddingLeft: 5,
    paddingRight: 25,
  },

  subtitulo_mis_vis: {
    fontSize: 17,
    fontWeight: "bold",
    color: "#4d4d4d",
    textAlign: "center",
    paddingTop: 14,
    paddingLeft: 5,
    paddingRight: 25,
  },
});

export default Pregrado_Vigente;
