import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import {
  Dialog,
  Portal,
  Card,
  Title,
  Button,
  Divider,
  Chip,
} from "react-native-paper";
import { Linking } from "react-native";
import Communications from "react-native-communications";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";
import config from "../../config";

class Asociaciones extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      empresas: null,
      contabilidad: null,
      finanzas: null,
      transporte: null,
      mercadotecnia: null,
      url_empresas: config.API_URL_API + "/asociaciones/1",
      url_contabilidad: config.API_URL_API + "/asociaciones/2",
      url_finanzas: config.API_URL_API + "/asociaciones/4",
      url_transporte: config.API_URL_API + "/asociaciones/5",
      url_mercadotecnia: config.API_URL_API + "/asociaciones/3",

      visible_empresas: false,
      visible_mis_vis_emp: false,

      visible_contabilidad: false,
      visible_mis_vis_cont: false,

      visible_finanzas: false,
      visible_mis_vis_fin: false,

      visible_transporte: false,
      visible_mis_vis_tra: false,

      visible_mercadotecnia: false,
      visible_mis_vis_mer: false,
    };
  }

  _showEmpresas = () => this.setState({ visible_empresas: true });
  _hideEmpresas = () => this.setState({ visible_empresas: false });
  _showMis_Vis_Emp = () => this.setState({ visible_mis_vis_emp: true });
  _hideMis_Vis_Emp = () => this.setState({ visible_mis_vis_emp: false });

  _showContabilidad = () => this.setState({ visible_contabilidad: true });
  _hideContabilidad = () => this.setState({ visible_contabilidad: false });
  _showMis_Vis_Cont = () => this.setState({ visible_mis_vis_cont: true });
  _hideMis_Vis_Cont = () => this.setState({ visible_mis_vis_cont: false });

  _showFinanzas = () => this.setState({ visible_finanzas: true });
  _hideFinanzas = () => this.setState({ visible_finanzas: false });
  _showMis_Vis_Fin = () => this.setState({ visible_mis_vis_fin: true });
  _hideMis_Vis_Fin = () => this.setState({ visible_mis_vis_fin: false });

  _showTransporte = () => this.setState({ visible_transporte: true });
  _hideTransporte = () => this.setState({ visible_transporte: false });
  _showMis_Vis_Tra = () => this.setState({ visible_mis_vis_tra: true });
  _hideMis_Vis_Tra = () => this.setState({ visible_mis_vis_tra: false });

  _showMercadotecnia = () => this.setState({ visible_mercadotecnia: true });
  _hideMercadotecnia = () => this.setState({ visible_mercadotecnia: false });
  _showMis_Vis_Mer = () => this.setState({ visible_mis_vis_mer: true });
  _hideMis_Vis_Mer = () => this.setState({ visible_mis_vis_mer: false });

  componentDidMount = () => {
    Promise.all([
      fetch(this.state.url_empresas),
      fetch(this.state.url_contabilidad),
      fetch(this.state.url_finanzas),
      fetch(this.state.url_transporte),
      fetch(this.state.url_mercadotecnia),
    ])
      .then((values) => {
        return Promise.all(values.map((r) => r.json()));
      })
      .then(([emp, cont, fin, trans, merc]) => {
        this.setState({
          empresas: emp.datos,
          contabilidad: cont.datos,
          finanzas: fin.datos,
          transporte: trans.datos,
          mercadotecnia: merc.datos,
          loading: false,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  };

  empresas() {
    return (
      <ScrollView>
        <View style={{ alignSelf: "center", paddingTop: 10 }}>
          <Card
            style={{ height: 65, width: 220, backgroundColor: "#ccd4e3"}}
            onPress={() => this._showMis_Vis_Emp()}
          >
            <Card.Content>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Misión / Visión
              </Title>
            </Card.Content>
          </Card>
        </View>

        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card style={{ height: 160, width: 290, backgroundColor: "#ccd4e3" }}>
            <Card.Content>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Contactos
              </Title>
              <Text style={styles.text}>
                <MaterialCommunityIconsIcon
                  name="phone"
                  size={18}
                  color={"black"}
                />
                Teléfono:
                <Text
                  style={styles.text_press}
                  onPress={() => 
                    Communications.phonecall(
                      this.state.empresas.telefono_asociacion,
                      true
                    )
                  }
                >
                  {this.state.empresas.telefono_asociacion}
                </Text>
                {"\n"}
                {"\n"}
                <MaterialCommunityIconsIcon
                  name="email"
                  size={18}
                  color={"black"}
                />
                Email:
                <Text
                  style={styles.text_press}
                  onPress={() =>
                    Communications.email(
                      ["", this.state.empresas.correo_asociacion],
                      null,
                      null,
                      "",
                      ""
                    )
                  }
                >
                  {this.state.empresas.correo_asociacion}
                </Text>
              </Text>
            </Card.Content>
          </Card>
        </View>
        <View style={{ alignItems: "center", paddingBottom: 10 }}>
          <Chip
            style={{ backgroundColor: "#f7faff", marginTop: 25 }}
            textStyle={{ fontSize: 15 }}
            height={34}
            width={110}
            mode="outlined"
            icon="facebook"
            selected="true"
            selectedColor="#3b5998"
            onPress={() =>
              Communications.web(this.state.empresas.facebook_asociacion)
            }
          >
            Síguenos
          </Chip>
        </View>
      </ScrollView>
    );
  }

  contabilidad() {
    return (
      <ScrollView>
        <View style={{ alignSelf: "center", paddingTop: 10 }}>
          <Card
            style={{ height: 65, width: 220, backgroundColor: "#ccd4e3"}}
            onPress={() => this._showMis_Vis_Cont()}
          >
            <Card.Content>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Misión / Visión
              </Title>
            </Card.Content>
          </Card>
        </View>

        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card style={{ height: 160, width: 290, backgroundColor: "#ccd4e3" }}>
            <Card.Content>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Contactos
              </Title>
              <Text style={styles.text}>
                <MaterialCommunityIconsIcon
                  name="phone"
                  size={18}
                  color={"black"}
                />
                Teléfono:
                <Text
                  style={styles.text_press}
                  onPress={() =>
                    Communications.phonecall(
                      this.state.contabilidad.telefono_asociacion,
                      true
                    )
                  }
                >
                  {this.state.contabilidad.telefono_asociacion}
                </Text>
                {"\n"}
                {"\n"}
                <MaterialCommunityIconsIcon
                  name="email"
                  size={18}
                  color={"black"}
                />
                Email:
                <Text
                  style={styles.text_press}
                  onPress={() =>
                    Communications.email(
                      ["", this.state.contabilidad.correo_asociacion],
                      null,
                      null,
                      "",
                      ""
                    )
                  }
                >
                  {this.state.contabilidad.correo_asociacion}
                </Text>
              </Text>
            </Card.Content>
          </Card>
        </View>
        <View style={{ alignItems: "center", paddingBottom: 10 }}>
          <Chip
            style={{ backgroundColor: "#f7faff", marginTop: 25 }}
            textStyle={{ fontSize: 15 }}
            height={34}
            width={110}
            mode="outlined"
            icon="facebook"
            selected="true"
            selectedColor="#3b5998"
            onPress={() =>
              Communications.web(this.state.contabilidad.facebook_asociacion)
            }
          >
            Síguenos
          </Chip>
        </View>
      </ScrollView>
    );
  }

  finanzas() {
    return (
      <ScrollView>
        <View style={{ alignSelf: "center", paddingTop: 10 }}>
          <Card
            style={{ height: 65, width: 220, backgroundColor: "#ccd4e3" }}
            onPress={() => this._showMis_Vis_Fin()}
          >
            <Card.Content>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Misión / Visión
              </Title>
            </Card.Content>
          </Card>
        </View>

        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card style={{ height: 160, width: 290, backgroundColor: "#ccd4e3" }}>
            <Card.Content>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Contactos
              </Title>
              <Text style={styles.text}>
                <MaterialCommunityIconsIcon
                  name="phone"
                  size={18}
                  color={"black"}
                />
                Teléfono:
                <Text
                  style={styles.text_press}
                  onPress={() =>
                    Communications.phonecall(
                      this.state.finanzas.telefono_asociacion,
                      true
                    )
                  }
                >
                  {this.state.finanzas.telefono_asociacion}
                </Text>
                {"\n"}
                {"\n"}
                <MaterialCommunityIconsIcon
                  name="email"
                  size={18}
                  color={"black"}
                />
                Email:
                <Text
                  style={styles.text_press}
                  onPress={() =>
                    Communications.email(
                      ["", this.state.finanzas.correo_asociacion],
                      null,
                      null,
                      "",
                      ""
                    )
                  }
                >
                  {this.state.finanzas.correo_asociacion}
                </Text>
              </Text>
            </Card.Content>
          </Card>
        </View>
        <View style={{ alignItems: "center", paddingBottom: 10 }}>
          <Chip
            style={{ backgroundColor: "#f7faff", marginTop: 25 }}
            textStyle={{ fontSize: 15 }}
            height={34}
            width={110}
            mode="outlined"
            icon="facebook"
            selected="true"
            selectedColor="#3b5998"
            onPress={() =>
              Communications.web(this.state.finanzas.facebook_asociacion)
            }
          >
            Síguenos
          </Chip>
        </View>
      </ScrollView>
    );
  }

  transporte() {
    return (
      <ScrollView>
        <View style={{ alignSelf: "center", paddingTop: 10 }}>
          <Card
            style={{ height: 65, width: 220, backgroundColor: "#ccd4e3" }}
            onPress={() => this._showMis_Vis_Tra()}
          >
            <Card.Content>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Misión / Visión
              </Title>
            </Card.Content>
          </Card>
        </View>

        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card style={{ height: 160, width: 290, backgroundColor: "#ccd4e3" }}>
            <Card.Content>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Contactos
              </Title>
              <Text style={styles.text}>
                <MaterialCommunityIconsIcon
                  name="phone"
                  size={18}
                  color={"black"}
                />
                Teléfono:
                <Text
                  style={styles.text_press}
                  onPress={() =>
                    Communications.phonecall(
                      this.state.transporte.telefono_asociacion,
                      true
                    )
                  }
                >
                  {this.state.transporte.telefono_asociacion}
                </Text>
                {"\n"}
                {"\n"}
                <MaterialCommunityIconsIcon
                  name="email"
                  size={18}
                  color={"black"}
                />
                Email:
                <Text
                  style={styles.text_press}
                  onPress={() =>
                    Communications.email(
                      ["", this.state.transporte.correo_asociacion],
                      null,
                      null,
                      "",
                      ""
                    )
                  }
                >
                  {this.state.transporte.correo_asociacion}
                </Text>
              </Text>
            </Card.Content>
          </Card>
        </View>
        <View style={{ alignItems: "center", paddingBottom: 10 }}>
          <Chip
            style={{ backgroundColor: "#f7faff", marginTop: 25 }}
            textStyle={{ fontSize: 15 }}
            height={34}
            width={110}
            mode="outlined"
            icon="facebook"
            selected="true"
            selectedColor="#3b5998"
            onPress={() =>
              Communications.web(this.state.transporte.facebook_asociacion)
            }
          >
            Síguenos
          </Chip>
        </View>
      </ScrollView>
    );
  }

  mercadotecnia() {
    return (
      <ScrollView>
        <View style={{ alignSelf: "center", paddingTop: 10 }}>
          <Card
            style={{ height: 65, width: 220, backgroundColor: "#ccd4e3" }}
            onPress={() => this._showMis_Vis_Mer()}
          >
            <Card.Content>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Misión / Visión
              </Title>
            </Card.Content>
          </Card>
        </View>

        <View style={{ alignSelf: "center", paddingTop: 20 }}>
          <Card style={{ height: 160, width: 290, backgroundColor: "#ccd4e3" }}>
            <Card.Content>
              <Title style={{ fontSize: 18, textAlign: "center" }}>
                Contactos
              </Title>
              <Text style={styles.text}>
                <MaterialCommunityIconsIcon
                  name="phone"
                  size={18}
                  color={"black"}
                />
                Teléfono:
                <Text
                  style={styles.text_press}
                  onPress={() =>
                    Communications.phonecall(
                      this.state.mercadotecnia.telefono_asociacion,
                      true
                    )
                  }
                >
                  {this.state.mercadotecnia.telefono_asociacion}
                </Text>
                {"\n"}
                {"\n"}
                <MaterialCommunityIconsIcon
                  name="email"
                  size={18}
                  color={"black"}
                />
                Email:
                <Text
                  style={styles.text_press}
                  onPress={() =>
                    Communications.email(
                      ["", this.state.mercadotecnia.correo_asociacion],
                      null,
                      null,
                      "",
                      ""
                    )
                  }
                >
                  {this.state.mercadotecnia.correo_asociacion}
                </Text>
              </Text>
            </Card.Content>
          </Card>
        </View>
        <View style={{ alignItems: "center", paddingBottom: 10 }}>
          <Chip
            style={{ backgroundColor: "#f7faff", marginTop: 25 }}
            textStyle={{ fontSize: 15 }}
            height={34}
            width={110}
            mode="outlined"
            icon="facebook"
            selected="true"
            selectedColor="#3b5998"
            onPress={() =>
              Communications.web(this.state.mercadotecnia.facebook_asociacion)
            }
          >
            Síguenos
          </Chip>
        </View>
      </ScrollView>
    );
  }

  mis_vis_empresas() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Misión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.empresas.mision_asociacion}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Visión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.empresas.vision_asociacion}
          </Text>
        </View>
      </ScrollView>
    );
  }

  mis_vis_contabilidad() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Misión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.contabilidad.mision_asociacion}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Visión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.contabilidad.vision_asociacion}
          </Text>
        </View>
      </ScrollView>
    );
  }

  mis_vis_finanzas() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Misión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.finanzas.mision_asociacion}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Visión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.finanzas.vision_asociacion}
          </Text>
        </View>
      </ScrollView>
    );
  }

  mis_vis_transporte() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Misión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.transporte.mision_asociacion}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Visión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.transporte.vision_asociacion}
          </Text>
        </View>
      </ScrollView>
    );
  }

  mis_vis_mercadotecnia() {
    return (
      <ScrollView>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Misión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.mercadotecnia.mision_asociacion}
          </Text>
        </View>
        <View>
          <Text style={styles.subtitulo_mis_vis}>Visión</Text>
          <Text style={styles.text_mis_vis}>
            {this.state.mercadotecnia.vision_asociacion}
          </Text>
        </View>
      </ScrollView>
    );
  }

  render() {
    const { loading } = this.state;
    if (!loading) {
      return (
        <View style={styles.container}>
          <ImageBackground
            source={require("../../assets/images/fade-transparente.png")}
            style={{ flex: 1, flexDirection: "column" }}
          >
            <ScrollView>
              <View style={styles.politicas_calidad}>
                <Text style={styles.titulo}>Asociaciones de Estudiantes</Text>
              </View>

              <View
                style={{
                  alignSelf: "center",
                  paddingTop: 20,
                  paddingBottom: 20,
                }}
              >
                <Card
                  style={{ height: 250, width: 330 }}
                  onPress={() => this._showEmpresas()}
                >
                  <Card.Cover
                    style={{ height: 180, width: 330, alignSelf: "center" }}
                    source={{
                      uri:
                        config.API_URL_ASO +
                        "/" +
                        this.state.empresas.logo_asociacion,
                    }}
                  />
                  <Card.Content style={{ alignSelf: "center" }}>
                    <Title style={{ fontSize: 17, textAlign: "center" }}>
                      {this.state.empresas.nombre_asociacion}
                    </Title>
                  </Card.Content>
                </Card>
              </View>

              <Divider />

              <View
                style={{
                  alignSelf: "center",
                  paddingTop: 20,
                  paddingBottom: 20,
                }}
              >
                <Card
                  style={{ height: 220, width: 330 }}
                  onPress={() => this._showContabilidad()}
                >
                  <Card.Cover
                    style={{
                      height: 180,
                      width: 330,
                      alignSelf: "center",
                    }}
                    source={{
                      uri:
                        config.API_URL_ASO +
                        "/" +
                        this.state.contabilidad.logo_asociacion,
                    }}
                  />
                  <Card.Content style={{ alignSelf: "center" }}>
                    <Title style={{ fontSize: 17, textAlign: "center" }}>
                      {this.state.contabilidad.nombre_asociacion}
                    </Title>
                  </Card.Content>
                </Card>
              </View>

              <Divider />

              <View
                style={{
                  alignSelf: "center",
                  paddingTop: 20,
                  paddingBottom: 20,
                }}
              >
                <Card
                  style={{ height: 220, width: 330 }}
                  onPress={() => this._showFinanzas()}
                >
                  <Card.Cover
                    style={{
                      height: 180,
                      width: 330,
                      alignSelf: "center",
                    }}
                    source={{
                      uri:
                        config.API_URL_ASO +
                        "/" +
                        this.state.finanzas.logo_asociacion,
                    }}
                  />
                  <Card.Content style={{ alignSelf: "center" }}>
                    <Title style={{ fontSize: 17, textAlign: "center" }}>
                      {this.state.finanzas.nombre_asociacion}
                    </Title>
                  </Card.Content>
                </Card>
              </View>

              <Divider />

              <View
                style={{
                  alignSelf: "center",
                  paddingTop: 20,
                  paddingBottom: 20,
                }}
              >
                <Card
                  style={{ height: 220, width: 330 }}
                  onPress={() => this._showTransporte()}
                >
                  <Card.Cover
                    style={{
                      height: 180,
                      width: 330,
                      alignSelf: "center",
                    }}
                    source={{
                      uri:
                        config.API_URL_ASO +
                        "/" +
                        this.state.transporte.logo_asociacion,
                    }}
                  />
                  <Card.Content style={{ alignSelf: "center" }}>
                    <Title style={{ fontSize: 17, textAlign: "center" }}>
                      {this.state.transporte.nombre_asociacion}
                    </Title>
                  </Card.Content>
                </Card>
              </View>

              <Divider />

              <View
                style={{
                  alignSelf: "center",
                  paddingTop: 20,
                  paddingBottom: 20,
                }}
              >
                <Card
                  style={{ height: 220, width: 330 }}
                  onPress={() => this._showMercadotecnia()}
                >
                  <Card.Cover
                    style={{
                      height: 180,
                      width: 330,
                      alignSelf: "center",
                    }}
                    source={{
                      uri:
                        config.API_URL_ASO +
                        "/" +
                        this.state.mercadotecnia.logo_asociacion,
                    }}
                  />
                  <Card.Content style={{ alignSelf: "center" }}>
                    <Title style={{ fontSize: 17, textAlign: "center" }}>
                      {this.state.mercadotecnia.nombre_asociacion}
                    </Title>
                  </Card.Content>
                </Card>
              </View>

              <Divider />

              {/*--------------------------------------------------EMPRESAS---------------------------------------------------------*/}
              <Portal>
                <Dialog
                  visible={this.state.visible_empresas}
                  onDismiss={this._hideEmpresas}
                  style={{ flex: 0.6 }}
                >
                  <Dialog.Title style={{ textAlign: "center", fontSize: 14 }}>
                    {this.state.empresas.nombre_asociacion}
                  </Dialog.Title>
                  <Dialog.ScrollArea>{this.empresas()}</Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideEmpresas}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_mis_vis_emp}
                  onDismiss={this._hideMis_Vis_Emp}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Misión / Visión
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.mis_vis_empresas()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideMis_Vis_Emp}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              {/*--------------------------------------------------CONTABILIDAD------------------------------------------------------*/}
              <Portal>
                <Dialog
                  visible={this.state.visible_contabilidad}
                  onDismiss={this._hideContabilidad}
                  style={{ flex: 0.6 }}
                >
                  <Dialog.Title style={{ textAlign: "center", fontSize: 16 }}>
                    {this.state.contabilidad.nombre_asociacion}
                  </Dialog.Title>
                  <Dialog.ScrollArea>{this.contabilidad()}</Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideContabilidad}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_mis_vis_cont}
                  onDismiss={this._hideMis_Vis_Cont}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Misión / Visión
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.mis_vis_contabilidad()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideMis_Vis_Cont}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              {/*--------------------------------------------------FINANZAS------------------------------------------------------*/}
              <Portal>
                <Dialog
                  visible={this.state.visible_finanzas}
                  onDismiss={this._hideFinanzas}
                  style={{ flex: 0.6 }}
                >
                  <Dialog.Title style={{ textAlign: "center", fontSize: 16 }}>
                    {this.state.finanzas.nombre_asociacion}
                  </Dialog.Title>
                  <Dialog.ScrollArea>{this.finanzas()}</Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideFinanzas}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_mis_vis_fin}
                  onDismiss={this._hideMis_Vis_Fin}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Misión / Visión
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.mis_vis_finanzas()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideMis_Vis_Fin}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              {/*--------------------------------------------------TRANSPORTE------------------------------------------------------*/}
              <Portal>
                <Dialog
                  visible={this.state.visible_transporte}
                  onDismiss={this._hideTransporte}
                  style={{ flex: 0.6 }}
                >
                  <Dialog.Title style={{ textAlign: "center", fontSize: 16 }}>
                    {this.state.transporte.nombre_asociacion}
                  </Dialog.Title>
                  <Dialog.ScrollArea>{this.transporte()}</Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideTransporte}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_mis_vis_tra}
                  onDismiss={this._hideMis_Vis_Tra}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Misión / Visión
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.mis_vis_transporte()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideMis_Vis_Tra}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              {/*------------------------------------------------MERCADOTECNIA------------------------------------------------------*/}
              <Portal>
                <Dialog
                  visible={this.state.visible_mercadotecnia}
                  onDismiss={this._hideMercadotecnia}
                  style={{ flex: 0.6 }}
                >
                  <Dialog.Title style={{ textAlign: "center", fontSize: 16 }}>
                    {this.state.mercadotecnia.nombre_asociacion}
                  </Dialog.Title>
                  <Dialog.ScrollArea>{this.mercadotecnia()}</Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideMercadotecnia}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>

              <Portal>
                <Dialog
                  visible={this.state.visible_mis_vis_mer}
                  onDismiss={this._hideMis_Vis_Mer}
                  style={{ flex: 0.7 }}
                >
                  <Dialog.Title style={{ textAlign: "center" }}>
                    Misión / Visión
                  </Dialog.Title>
                  <Dialog.ScrollArea>
                    {this.mis_vis_mercadotecnia()}
                  </Dialog.ScrollArea>
                  <Dialog.Actions>
                    <Button
                      onPress={this._hideMis_Vis_Mer}
                      labelStyle={{ color: "#344a72" }}
                    >
                      Cerrar
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>
            </ScrollView>
          </ImageBackground>
        </View>
      );
    } else {
      return (
        <ImageBackground
          source={require("../../assets/images/fade-transparente.png")}
          style={{ flex: 1, flexDirection: "column", justifyContent: "center" }}
        >
          <ActivityIndicator
            size="large"
            color="#344a72"
            justifyContent="space-around"
          />
        </ImageBackground>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingTop: 0,
    flexDirection: "column",
  },
  text: {
    fontSize: 17,
    fontFamily: "antic-slab",
    color: "#4d4d4d",
    textAlign: "justify",
  },
  text_press: {
    fontSize: 17,
    fontFamily: "antic-slab",
    color: "#4d4d4d",
    textAlign: "center",
    textDecorationLine: "underline",
  },

  titulo: {
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
    fontWeight: "bold",
    fontSize: 30,
    color: "#344a72",
    textAlign: "center",
  },
  text_mis_vis: {
    fontSize: 17,
    fontFamily: "antic-slab",
    color: "#4d4d4d",
    textAlign: "justify",
    paddingTop: 14,
    paddingLeft: 5,
    paddingRight: 25,
  },
  subtitulo_mis_vis: {
    fontSize: 17,
    fontWeight: "bold",
    color: "#4d4d4d",
    textAlign: "center",
    paddingTop: 14,
    paddingLeft: 5,
    paddingRight: 25,
  },
});

export default Asociaciones;
