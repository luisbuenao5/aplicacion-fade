import * as React from "react";
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Pregado_Vigente from "./Pregrado_Vigente";
import Asociaciones from "./Asociaciones";


const Tab3 = createMaterialTopTabNavigator ();

export default function AppNavigatorAcademia() {
  return (
    <Tab3.Navigator
    screenOptions={{       
      
      tabBarActiveTintColor: "#ffffff",
      tabBarInactiveTintColor: "#ccd4e3",
      tabBarIndicatorStyle: {
        backgroundColor: "#ffffff"
      },
      tabBarLabelStyle: {
        fontSize: 10, 
      },
      tabBarStyle: { 
        backgroundColor: "#293A59"
      },
      
    }}
    
    >
      <Tab3.Screen name="Pregrado Vigente" component={Pregado_Vigente} />      
      <Tab3.Screen name="Asociaciones" component={Asociaciones} />
    </Tab3.Navigator>
  );
}
