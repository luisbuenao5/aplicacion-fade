import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import {
  Card,
  Title,
  Paragraph,
} from "react-native-paper";
import config from "../../config";

class Autoridades extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      decana: null,
      tipo_decana: null,
      vicedecano: null,
      tipo_vicedecano: null,
      empresas: null,
      tipo_empresas: null,
      financiera: null,
      tipo_financiera: null,
      marketing: null,
      tipo_marketing: null,
      contabilidad: null,
      tipo_contabilidad: null,
      transportes: null,
      tipo_transportes: null,
      distancia: null,
      tipo_distancia: null,

      url_decana: config.API_URL_API + "/autoridades/1",
      url_vicedecano: config.API_URL_API + "/autoridades/2",
      url_empresas: config.API_URL_API + "/autoridades/3",
      url_financiera: config.API_URL_API + "/autoridades/4",
      url_marketing: config.API_URL_API + "/autoridades/5",
      url_contabilidad: config.API_URL_API + "/autoridades/6",
      url_transportes: config.API_URL_API + "/autoridades/8",
      url_distancia: config.API_URL_API + "/autoridades/9",

      url_tipo_decana: config.API_URL_API + "/tipo_autoridades/8",
      url_tipo_vicedecano: config.API_URL_API + "/tipo_autoridades/13",
      url_tipo_empresas: config.API_URL_API + "/tipo_autoridades/4",
      url_tipo_financiera: config.API_URL_API + "/tipo_autoridades/10",
      url_tipo_marketing: config.API_URL_API + "/tipo_autoridades/9",
      url_tipo_contabilidad: config.API_URL_API + "/tipo_autoridades/3",
      url_tipo_transportes: config.API_URL_API + "/tipo_autoridades/11",
      url_tipo_distancia: config.API_URL_API + "/tipo_autoridades/14",
    };
  }

  componentDidMount = () => {
    Promise.all([
      fetch(this.state.url_decana),
      fetch(this.state.url_vicedecano),
      fetch(this.state.url_empresas),
      fetch(this.state.url_financiera),
      fetch(this.state.url_marketing),
      fetch(this.state.url_contabilidad),
      fetch(this.state.url_transportes),
      fetch(this.state.url_distancia),
      fetch(this.state.url_tipo_decana),
      fetch(this.state.url_tipo_vicedecano),
      fetch(this.state.url_tipo_empresas),
      fetch(this.state.url_tipo_financiera),
      fetch(this.state.url_tipo_marketing),
      fetch(this.state.url_tipo_contabilidad),
      fetch(this.state.url_tipo_transportes),
      fetch(this.state.url_tipo_distancia),
    ])
      .then((values) => {
        return Promise.all(values.map((r) => r.json()));
      })
      .then(
        ([
          dec,
          vice,
          emp,
          fin,
          mark,
          cont,
          tran,
          dist,
          tipo_dec,
          tipo_vice,
          tipo_emp,
          tipo_fin,
          tipo_mark,
          tipo_cont,
          tipo_tran,
          tipo_dist,
        ]) => {
          this.setState({
            decana: dec.datos,
            vicedecano: vice.datos,
            empresas: emp.datos,
            financiera: fin.datos,
            marketing: mark.datos,
            contabilidad: cont.datos,
            transportes: tran.datos,
            distancia: dist.datos,

            tipo_decana: tipo_dec.datos,
            tipo_vicedecano: tipo_vice.datos,
            tipo_empresas: tipo_emp.datos,
            tipo_financiera: tipo_fin.datos,
            tipo_marketing: tipo_mark.datos,
            tipo_contabilidad: tipo_cont.datos,
            tipo_transportes: tipo_tran.datos,
            tipo_distancia: tipo_dist.datos,
            loading: false,
          });
        }
      )
      .catch((error) => {
        console.error(error);
      });
  };

  render() {
    const { loading } = this.state;
    if (!loading) {
      return (
        <View style={styles.container}>
          <ImageBackground
            source={require("../../assets/images/fade-transparente.png")}
            style={{ flex: 1, flexDirection: "column" }}
          >
            <ScrollView>
              <View style={styles.autoridades}>
                <Text style={styles.titulo}>Autoridades de la Facultad</Text>
              </View>

              <View style={{ alignSelf: "center", paddingTop: 20 }}>
                <Card style={{ height: 270, width: 260, paddingTop: 10 }}>
                  <Card.Cover
                    style={{ height: 190, width: 170, alignSelf: "center" }}
                    source={{
                      uri:
                        config.API_URL_AUT +
                        "/" +
                        this.state.decana.foto_autoridad,
                    }}
                  />
                  <Card.Content style={{ alignSelf: "center" }}>
                    <Paragraph
                      style={{ fontSize: 16,
                        textAlign: "center", fontFamily: "antic-slab" }}
                    >
                      {this.state.decana.nombres_autoridad +
                        " " +
                        this.state.decana.apellidos_autoridad}
                    </Paragraph>
                    <Title style={{ fontSize: 18, alignSelf: "center" }}>
                      {this.state.tipo_decana.descripcion_tipo_autoridad}
                    </Title>
                  </Card.Content>
                </Card>
              </View>

              <View style={{ alignSelf: "center", paddingTop: 30 }}>
                <Card style={{ height: 270, width: 260, paddingTop: 10 }}>
                  <Card.Cover
                    style={{ height: 190, width: 170, alignSelf: "center" }}
                    source={{
                      uri:
                        config.API_URL_AUT +
                        "/" +
                        this.state.vicedecano.foto_autoridad,
                    }}
                  />
                  <Card.Content style={{ alignSelf: "center" }}>
                    <Paragraph
                      style={{ fontSize: 16, 
                        textAlign: "center", fontFamily: "antic-slab" }}
                    >
                      {this.state.vicedecano.nombres_autoridad +
                        " " +
                        this.state.vicedecano.apellidos_autoridad}
                    </Paragraph>
                    <Title style={{ fontSize: 18, alignSelf: "center" }}>
                      {this.state.tipo_vicedecano.descripcion_tipo_autoridad}
                    </Title>
                  </Card.Content>
                </Card>
              </View>

              <View style={{ alignSelf: "center", paddingTop: 30 }}>
                <Card style={{ height: 320, width: 260, paddingTop: 10 }}>
                  <Card.Cover
                    style={{ height: 190, width: 170, alignSelf: "center" }}
                    source={{
                      uri:
                        config.API_URL_AUT +
                        "/" +
                        this.state.empresas.foto_autoridad,
                    }}
                  />
                  <Card.Content style={{ alignSelf: "center" }}>
                    <Paragraph
                      style={{
                        fontSize: 16,
                        textAlign: "center",
                        fontFamily: "antic-slab",
                      }}
                    >
                      {this.state.empresas.nombres_autoridad +
                        " " +
                        this.state.empresas.apellidos_autoridad}
                    </Paragraph>
                    <Title style={{ fontSize: 18, textAlign: "center" }}>
                      {this.state.tipo_empresas.descripcion_tipo_autoridad}
                    </Title>
                  </Card.Content>
                </Card>
              </View>

              <View style={{ alignSelf: "center", paddingTop: 30 }}>
                <Card style={{ height: 310, width: 260, paddingTop: 10 }}>
                  <Card.Cover
                    style={{ height: 170, width: 140, alignSelf: "center" }}
                    source={{
                      uri:
                        config.API_URL_AUT +
                        "/" +
                        this.state.financiera.foto_autoridad,
                    }}
                  />
                  <Card.Content style={{ alignSelf: "center" }}>
                    <Paragraph
                      style={{
                        fontSize: 16,
                        textAlign: "center",
                        fontFamily: "antic-slab",
                      }}
                    >
                      {this.state.financiera.nombres_autoridad +
                        " " +
                        this.state.financiera.apellidos_autoridad}
                    </Paragraph>
                    <Title style={{ fontSize: 18, textAlign: "center" }}>
                      {this.state.tipo_financiera.descripcion_tipo_autoridad}
                    </Title>
                  </Card.Content>
                </Card>
              </View>

              <View style={{ alignSelf: "center", paddingTop: 30 }}>
                <Card style={{ height: 310, width: 260, paddingTop: 10 }}>
                  <Card.Cover
                    style={{ height: 190, width: 170, alignSelf: "center" }}
                    source={{
                      uri:
                        config.API_URL_AUT +
                        "/" +
                        this.state.marketing.foto_autoridad,
                    }}
                  />
                  <Card.Content style={{ alignSelf: "center" }}>
                    <Paragraph
                      style={{
                        fontSize: 16,
                        textAlign: "center",
                        fontFamily: "antic-slab",
                      }}
                    >
                      {this.state.marketing.nombres_autoridad +
                        " " +
                        this.state.marketing.apellidos_autoridad}
                    </Paragraph>
                    <Title style={{ fontSize: 18, textAlign: "center" }}>
                      {this.state.tipo_marketing.descripcion_tipo_autoridad}
                    </Title>
                  </Card.Content>
                </Card>
              </View>

              <View style={{ alignSelf: "center", paddingTop: 30 }}>
                <Card style={{ height: 340, width: 260, paddingTop: 10 }}>
                  <Card.Cover
                    style={{ height: 190, width: 170, alignSelf: "center" }}
                    source={{
                      uri:
                        config.API_URL_AUT +
                        "/" +
                        this.state.contabilidad.foto_autoridad,
                    }}
                  />
                  <Card.Content style={{ alignSelf: "center" }}>
                    <Paragraph
                      style={{
                        fontSize: 16,
                        textAlign: "center",
                        fontFamily: "antic-slab",
                      }}
                    >
                      {this.state.contabilidad.nombres_autoridad +
                        " " +
                        this.state.contabilidad.apellidos_autoridad}
                    </Paragraph>
                    <Title style={{ fontSize: 18, textAlign: "center" }}>
                      {this.state.tipo_contabilidad.descripcion_tipo_autoridad}
                    </Title>
                  </Card.Content>
                </Card>
              </View>
              <View style={{ alignSelf: "center", paddingTop: 30 }}>
                <Card style={{ height: 330, width: 260, paddingTop: 10 }}>
                  <Card.Cover
                    style={{ height: 190, width: 170, alignSelf: "center" }}
                    source={{
                      uri:
                        config.API_URL_AUT +
                        "/" +
                        this.state.transportes.foto_autoridad,
                    }}
                  />
                  <Card.Content style={{ alignSelf: "center" }}>
                    <Paragraph
                      style={{
                        fontSize: 16,
                        textAlign: "center",
                        fontFamily: "antic-slab",
                      }}
                    >
                      {this.state.transportes.nombres_autoridad +
                        " " +
                        this.state.transportes.apellidos_autoridad}
                    </Paragraph>
                    <Title style={{ fontSize: 18, textAlign: "center" }}>
                      {this.state.tipo_transportes.descripcion_tipo_autoridad}
                    </Title>
                  </Card.Content>
                </Card>
              </View>

              <View
                style={{
                  alignSelf: "center",
                  paddingTop: 30,
                  paddingBottom: 20,
                }}
              >
                <Card style={{ height: 320, width: 260, paddingTop: 10 }}>
                  <Card.Cover
                    style={{ height: 190, width: 170, alignSelf: "center" }}
                    source={{
                      uri:
                        config.API_URL_AUT +
                        "/" +
                        this.state.distancia.foto_autoridad,
                    }}
                  />
                  <Card.Content style={{ alignSelf: "center" }}>
                    <Paragraph
                      style={{
                        fontSize: 16,
                        textAlign: "center",
                        fontFamily: "antic-slab",
                      }}
                    >
                      {this.state.distancia.nombres_autoridad +
                        " " +
                        this.state.distancia.apellidos_autoridad}
                    </Paragraph>
                    <Title style={{ fontSize: 18, textAlign: "center" }}>
                      {this.state.tipo_distancia.descripcion_tipo_autoridad}
                    </Title>
                  </Card.Content>
                </Card>
              </View>
            </ScrollView>
          </ImageBackground>
        </View>
      );
    } else {
      return (
        <ImageBackground
          source={require("../../assets/images/fade-transparente.png")}
          style={{ flex: 1, flexDirection: "column", justifyContent: "center" }}
        >
          <ActivityIndicator
            size="large"
            color="#344a72"
            justifyContent="space-around"
          />
        </ImageBackground>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingTop: 0,
    flexDirection: "column",
  },
  text3: {
    fontSize: 18,
    color: "#758eba",
    textAlign: "justify",
    paddingTop: 20,
    paddingLeft: 7,
    paddingRight: 7,
  },
  autoridades: {
    flex: 1,
    flexDirection: "row",
    marginTop: 5,
    marginLeft: 5,
    alignSelf: "center",
  },

  titulo: {
    paddingTop: 15,
    fontWeight: "bold",
    fontSize: 25,
    color: "#344a72",
  },
  text_press: {
    fontSize: 25,
    fontFamily: "antic-slab",
    color: "#4d4d4d",
    textAlign: "center",
    textDecorationLine: "underline",
    paddingTop: 14,
    paddingLeft: 5,
    paddingRight: 25,
  },
});

export default Autoridades;
