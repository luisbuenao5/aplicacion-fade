import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  ActivityIndicator,
  Alert,
} from "react-native";
import {
  Appbar,
  Provider as PaperProvider,
  Button,
  Paragraph,
  Dialog,
  Portal,
} from "react-native-paper";
import { TextInput } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Dropdown } from "react-native-material-dropdown-v2";
import config from "../config";


class Quejas extends Component {
    constructor(props) {
        super(props);
        this.state = {
          loading: true,
          datos: null,
          tipo_queja: "",
          escuela_pertenece: "",
          descripcion_queja: "",
          visible: false,
          visible2: false,
          visible3: false,
          url: config.API_URL_CED,
        };
      }
async enviar_api() {
        try {
          await fetch(config.API_URL_API + "/quejas", {
            method: "post",
            mode: "no-cors",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              tipo_queja: this.state.tipo_queja,
              escuela_pertenece: this.state.escuela_pertenece,
              descripcion_queja: this.state.descripcion_queja,
              cedula_usuario: this.props.navigation.getParam("cedula"),
              nomb_apell_usuario:
                this.state.datos.per_nombres +
                " " +
                this.state.datos.per_primerApellido +
                " " +
                this.state.datos.per_segundoApellido,
              correo_usuario: this.state.datos.per_email,
            }),
          });
        } catch (e) {
          Alert.alert("Su comentario no fue enviado!");
          this.setState({ visible3: false });
        }
      }
    
      async obtener_cedula() {
        await fetch(this.state.url + this.props.navigation.getParam("cedula"), {
          method: "GET",
        })
          .then((res) => res.json())
          .then((res) => {
            this.setState({
              datos: res,
              loading: false,
            });
          })
          .catch((error) => {
            Alert.alert("¡Cédula no válida!");
          });
      }
    
componentDidMount() {
this.obtener_cedula();
}
_showDialog = () => this.setState({ visible: true });

_hideDialog = () => this.setState({ visible: false });

_showDialog2 = () => this.setState({ visible2: true });

_hideDialog2 = () => this.setState({ visible2: false });

_showDialog3 = () => this.setState({ visible3: true });

_hideDialog3 = () => this.setState({ visible3: false });

enviar() {
    this.enviar_api();
    this._showDialog3();
    this._hideDialog();
    this.textFieldComentario.clear();
    this.setState({ tipo_queja: "" });
    this.setState({ escuela_pertenece: "" });
    this.setState({ descripcion_queja: "" });
  }

comprobar() {
    if (
      this.state.tipo_queja &&
      this.state.escuela_pertenece &&
      this.state.descripcion_queja &&
      this.state.descripcion_queja.length <= 600
    ) {
      this._showDialog();
    } else {
      this._showDialog2();
    }
  }


  render() {
    let escuelas = [
        {
          value: "Decanato",
        },
        {
          value: "Subdecanato",
        },
        {
          value: "Administración de Empresas",
        },
        {
          value: "Contabilidad y Auditoría",
        },
        {
          value: "Finanzas",
        },
        {
          value: "Gestión de Transporte",
        },
        {
          value: "Mercadotecnia",
        },
        {
          value: "Otro",
        },
      ];
  
      let tipos = [
        {
          value: "Queja",
        },
        {
          value: "Reclamo",
        },
        {
          value: "Sugerencia",
        },
        {
          value: "Otro",
        },
      ];

/*const { loading } = this.state;
if (!loading) { 
*/
    return (
      <View style={styles.container}>
        <PaperProvider>
            <Appbar.Header style={{ backgroundColor: "#172133" }}>
                <Appbar.BackAction
                        onPress={() => this.props.navigation.goBack()}
                />
                <Appbar.Content 
                    title="Comentarios"
                    subtitle="Quejas/Reclamos/Sugerencias" 
                />                  
            </Appbar.Header>

            <KeyboardAwareScrollView>
                <View style={styles.escuela}>
                    <Text style={styles.text}> 
                        ¡Hola {""}
                        <Text style={{ fontWeight: "bold" }}>
                            
                        </Text>
                            ! el siguiente formulario te permitirá registrar una queja,
                            reclamo o sugerencia, que creas conveniente sobre una unidad o
                            una escuela en específico.
                    </Text>
                    <Text style={styles.text} >
                     1. ¿Sobre qué unidad quiere registrar el comentario?
                    </Text>
                    <Dropdown
                        placeholder="Elija la Unidad"
                        data={escuelas}
                        animationDuration={0}
                        value={this.state.escuela_pertenece}
                        onChangeText={(escuela) =>
                        this.setState({ escuela_pertenece: escuela })
                        }
                        containerStyle={{ paddingLeft: 10, paddingRight: 10 }}
                    />
                </View>

                <View>
                    <Text style={styles.text} >  2. Tipo de comentario</Text>
                
                    <Dropdown
                        placeholder="Elija el Tipo de Comentario"
                        data={tipos}
                        animationDuration={0}
                        value={this.state.tipo_queja}
                        onChangeText={(tipo) => this.setState({ tipo_queja: tipo })}
                        containerStyle={{ paddingLeft: 10, paddingRight: 20 }}
                    />         
                </View>              

                <View>
                    <Text style={styles.text}>  3. Comentario</Text>
                    <TextInput
                        ref={(input) => {
                            this.textFieldComentario = input;
                        }}
                        placeholder ="Escriba Aqui..."
                        style={styles.input}
                        textAlignVertical="auto"
                        multiline={true}
                        value={this.state.descripcion_queja}
                        onChangeText={(text) =>
                            this.setState({ descripcion_queja: text })
                        }
                        characterRestriction={600}
                        maxLength={600}
                    />
                </View>


                <View style={styles.boton}>
                    <Button
                            onPress={() => this.comprobar()}
                        label="Enviar"
                            labelStyle={{ fontSize: 20 }}
                        mode="contained"
                        color="#344a72"
                    >
                        Enviar
                    </Button>
                    <Portal>
                        <Dialog
                            visible={this.state.visible}
                            onDismiss={this._hideDialog}
                        >
                            <Dialog.Title style={{ textAlign: "center", fontSize: 20 }}>
                                ¿Está completamente seguro/a?
                            </Dialog.Title>
                            <Dialog.Content>
                                <Paragraph>
                                    Una vez enviado no será posible editar el comentario
                                </Paragraph>
                            </Dialog.Content>
                            <Dialog.Actions>
                                <Button
                                    onPress={this._hideDialog}
                                    labelStyle={{ color: "#344a72" }}
                                >
                                    Cancelar
                                </Button>
                                <Button
                                    onPress={() => this.enviar()}
                                    labelStyle={{ color: "#344a72" }}
                                >
                                    Sí, estoy seguro/a
                                </Button>
                            </Dialog.Actions>
                        </Dialog>
                    </Portal>

                    <Portal>
                        <Dialog
                            visible={this.state.visible2}
                            onDismiss={this._hideDialog2}
                        >
                            <Dialog.Title style={{ textAlign: "center", fontSize: 23 }}>
                                ¡Por favor complete todos los campos!
                            </Dialog.Title>
                            <Dialog.Actions>
                                <Button
                                    onPress={this._hideDialog2}
                                    labelStyle={{ color: "#344a72", fontSize: 15 }}
                                >
                                    OK
                                </Button>
                            </Dialog.Actions>
                        </Dialog>
                    </Portal>

                    <Portal>
                        <Dialog
                            visible={this.state.visible3}
                            onDismiss={this._hideDialog3}
                        >
                            <Dialog.Title style={{ textAlign: "center", fontSize: 23 }}>
                                ¡Su comentario ha sido enviado correctamente!
                            </Dialog.Title>
                            <Dialog.Actions>
                                <Button
                                    onPress={this._hideDialog3}
                                    labelStyle={{ color: "#344a72", fontSize: 15 }}
                                >
                                    OK
                                </Button>
                            </Dialog.Actions>
                        </Dialog>
                    </Portal>
                </View>
            </KeyboardAwareScrollView>
       </PaperProvider>
      </View>
    );
  } 
 /* else {
    return (
      <View style={{ flex: 1, justifyContent: "center" }}>
        <ActivityIndicator
          size="large"
          color="#344a72"
          justifyContent="space-around"
        />
      </View>
    );
  }
 }*/
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "white",
      flexDirection: "column",
    },
    input: {

      height: 60,
      margin: 12,
      borderWidth: 1,
      padding: 11,
    },
    escuela: {
      paddingLeft: 10,
      paddingRight: 10,
    },
    ingreso: {
      paddingLeft: 10,
      paddingRight: 10,
      paddingBottom: 50,
    },
    cedula: {
      paddingLeft: 10,
      paddingRight: 10,
    },
    nombres: {
      paddingLeft: 10,
      paddingRight: 10,
    },
    correo: {
      paddingTop: 30,
      paddingLeft: 10,
      paddingRight: 10,
      paddingBottom: 50,
    },
    picker: {
      paddingLeft: 10,
    },
    text: {
      marginTop: 20,
      fontSize: 17,
      color: "#4d4d4d",
      textAlign: "justify",
    },
    boton: {
      paddingTop: 50,
      paddingLeft: 10,
      paddingRight: 10,
      paddingBottom: 160,
    },
  });

export default Quejas;