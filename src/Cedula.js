import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  ActivityIndicator,
  Dimensions,
  Alert,
} from "react-native";
import {
  Appbar,
  Provider as PaperProvider,
  HelperText,
  Button,
  Paragraph,
  Dialog,
  Portal,
} from "react-native-paper";
import { TextInput } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import config from "../config";

class Cedula extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      validar_cedula: false,
      datos: null,
      cedula_usuario: "",
      visible: false,
      visible2: false,
      visible3: false,
      url: config.API_URL_CED,
    };
  }

  _showDialog = () => this.setState({ visible: true });

  _hideDialog = () => this.setState({ visible: false });

  _showDialog2 = () => this.setState({ visible2: true });

  _hideDialog2 = () => this.setState({ visible2: false });

  _showDialog3 = () => this.setState({ visible3: true });

  _hideDialog3 = () => this.setState({ visible3: false });

  continuar() {
    if (this.state.datos.per_email) {
      this.props.navigation.navigate("Quejas", {
        cedula: this.state.cedula_usuario,
      });
      this.setState({ cedula_usuario: "" });
      this.setState({ datos: null });
      this.textFieldCedula.clear();
    } else {
      this._showDialog();
    }
  }

  comprobar() {
    if (
      this.state.cedula_usuario &&
      this.state.cedula_usuario.length == 10 &&
      this.state.validar_cedula == false
    ) {
      this.continuar();
    } else if (this.state.cedula_usuario.length == 10) {
      this._showDialog3();
    } else {
      this._showDialog2();
    }
  }

  validar_cedula(text) {
    var cedula = text;
    //Obtenemos el digito de la region que son los dos primeros digitos
    var digito_region = cedula.substring(0, 2);

    //Pregunto si la region existe ecuador se divide en 24 regiones
    if (digito_region >= 1 && digito_region <= 24) {
      // Extraigo el ultimo digito
      var ultimo_digito = cedula.substring(9, 10);

      //Agrupo todos los pares y los sumo
      var pares =
        parseInt(cedula.substring(1, 2)) +
        parseInt(cedula.substring(3, 4)) +
        parseInt(cedula.substring(5, 6)) +
        parseInt(cedula.substring(7, 8));

      //Agrupo los impares, los multiplico por un factor de 2, si la resultante es > que 9 le restamos el 9 a la resultante
      var numero1 = cedula.substring(0, 1);
      var numero1 = numero1 * 2;
      if (numero1 > 9) {
        var numero1 = numero1 - 9;
      }

      var numero3 = cedula.substring(2, 3);
      var numero3 = numero3 * 2;
      if (numero3 > 9) {
        var numero3 = numero3 - 9;
      }

      var numero5 = cedula.substring(4, 5);
      var numero5 = numero5 * 2;
      if (numero5 > 9) {
        var numero5 = numero5 - 9;
      }

      var numero7 = cedula.substring(6, 7);
      var numero7 = numero7 * 2;
      if (numero7 > 9) {
        var numero7 = numero7 - 9;
      }

      var numero9 = cedula.substring(8, 9);
      var numero9 = numero9 * 2;
      if (numero9 > 9) {
        var numero9 = numero9 - 9;
      }

      var impares = numero1 + numero3 + numero5 + numero7 + numero9;

      //Suma total
      var suma_total = pares + impares;

      //extraemos el primero digito
      var primer_digito_suma = String(suma_total).substring(0, 1);

      //Obtenemos la decena inmediata
      var decena = (parseInt(primer_digito_suma) + 1) * 10;

      //Obtenemos la resta de la decena inmediata - la suma_total esto nos da el digito validador
      var digito_validador = decena - suma_total;

      //Si el digito validador es = a 10 toma el valor de 0
      if (digito_validador == 10) var digito_validador = 0;

      //Validamos que el digito validador sea igual al de la cedula
      if (digito_validador == ultimo_digito) {
        this.setState({ validar_cedula: false });
      } else {
        this.setState({ validar_cedula: true });
      }
    } else {
      // si la region no pertenece
      this.setState({ validar_cedula: true });
    }
  }

  onChanged(text) {
    let newText = "";
    let numbers = "0123456789";

    for (var i = 0; i < text.length; i++) {
      if (numbers.indexOf(text[i]) > -1) {
        newText = newText + text[i];
      } else {
        Alert.alert("Ingrese solamente números");
      }
    }
    this.setState({ cedula_usuario: newText });
    this.validar_cedula(newText);

    if (newText.length == 10) {
      fetch(this.state.url + text, { method: "GET" })
        .then((res) => res.json())
        .then((res) => {
          this.setState({
            datos: res,
          });
        })
        .catch((error) => {
          this.setState({ validar_cedula: true });
        });
    } else {
      this.setState({ datos: null });
    }
  }

  render() {
    const { loading } = this.state;
    if (loading) {
      return (
        <View style={styles.container}>
          <PaperProvider>
            <Appbar.Header style={{ backgroundColor: "#172133" }}>
              <Appbar.BackAction
                onPress={() => this.props.navigation.goBack()}
              />
              <Appbar.Content
                title="Comentarios"
                subtitle="Ingreso de cédula"
              />
            </Appbar.Header>

            <KeyboardAwareScrollView extraHeight={50}>
              <View style={styles.ingreso}>
                <Text style={styles.text}>
                  Ingrese su número de cédula{"\n"}
                </Text>
                <View style={styles.cedula}>
                <TextInput
                    ref={(input) => {
                        this.textFieldCedula = input;
                    }}
                    style={styles.input}
                    placeholder="Ej: 1234567890"
                    keyboardType="numeric"
                    maxLength={10}
                    characterRestriction={10}
                    value={this.state.cedula_usuario}
                    onChangeText={(cedula) => this.onChanged(cedula)}
                  />
                  <HelperText type="error" visible={this.state.validar_cedula}>
                    Cédula no válida
                  </HelperText>
                </View>
                <Text style={styles.text2}>
                  *Nota: El ingreso de su número de cédula es con el único fin
                  de verificar si usted pertenece a la Escuela Superior
                  Politécnica de Chimborazo, y de ser ese el caso se obtendrán
                  sus nombres y correo electrónico registrados para dar así un
                  seguimiento al comentario enviado.
                </Text>
                <View style={styles.boton}>
                  <Button
                    onPress={() => this.comprobar()}
                    label="Continuar"
                    labelStyle={{
                      fontSize: Dimensions.get("window").height * 0.025,
                    }}
                    mode="contained"
                    color="#344a72"
                  >
                    Continuar
                  </Button>

                  <Portal>
                    <Dialog
                      visible={this.state.visible}
                      onDismiss={this._hideDialog}
                    >
                      <Dialog.Title
                        style={{ textAlign: "center", fontSize: 22 }}
                      >
                        ¡La cédula ingresada no coincide con los registros de la
                        ESPOCH!
                      </Dialog.Title>
                      <Dialog.Actions>
                        <Button
                          onPress={this._hideDialog}
                          labelStyle={{ color: "#344a72", fontSize: 15 }}
                        >
                          OK
                        </Button>
                      </Dialog.Actions>
                    </Dialog>
                  </Portal>

                  <Portal>
                    <Dialog
                      visible={this.state.visible2}
                      onDismiss={this._hideDialog2}
                    >
                      <Dialog.Title
                        style={{ textAlign: "center", fontSize: 23 }}
                      >
                        ¡Por favor complete el campo cédula!
                      </Dialog.Title>
                      <Dialog.Actions>
                        <Button
                          onPress={this._hideDialog2}
                          labelStyle={{ color: "#344a72", fontSize: 15 }}
                        >
                          OK
                        </Button>
                      </Dialog.Actions>
                    </Dialog>
                  </Portal>

                  <Portal>
                    <Dialog
                      visible={this.state.visible3}
                      onDismiss={this._hideDialog3}
                    >
                      <Dialog.Title
                        style={{ textAlign: "center", fontSize: 23 }}
                      >
                        ¡Por favor corrija el campo cédula!
                      </Dialog.Title>
                      <Dialog.Actions>
                        <Button
                          onPress={this._hideDialog3}
                          labelStyle={{ color: "#344a72", fontSize: 15 }}
                        >
                          OK
                        </Button>
                      </Dialog.Actions>
                    </Dialog>
                  </Portal>
                </View>
              </View>
            </KeyboardAwareScrollView>
          </PaperProvider>
        </View>
      );
    } else {
      return (
        <View style={{ flex: 1, justifyContent: "center" }}>
          <ActivityIndicator
            size="large"
            color="#344a72"
            justifyContent="space-around"
          />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    flexDirection: "column",
  },
  ingreso: {
    paddingTop: Dimensions.get("window").width * 0.5,
  },
  input: {

    height: 60,
    margin: 5,
    borderWidth: 1,
    padding: 15,
  },
  cedula: {
    paddingLeft: 100,
    paddingRight: 100,
  },
  text: {
    marginTop: 25,
    fontSize: 18,
    alignSelf: "center",
    color: "#4d4d4d",
    textAlign: "justify",
  },
  text2: {
    marginTop: 25,
    fontSize: 12,
    alignSelf: "center",
    color: "#4d4d4d",
    textAlign: "justify",
    paddingLeft: 10,
    paddingRight: 10,
  },
  boton: {
    paddingTop: 30,
    paddingLeft: 110,
    paddingRight: 110,
    paddingBottom: 150,
  },
});

export default Cedula;
