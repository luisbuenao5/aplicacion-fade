import * as React from "react";
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Politica_Calidad from './Politica_Calidad';
import Mapa_Procesos from './Mapa_Procesos';
import Mapa_Estrategico from './Mapa_Estrategico';
import Infraestructura from './Infraestructura';

const Tab2 = createMaterialTopTabNavigator();

export default function AppNavigatorGestion() {
  return (
    <Tab2.Navigator
    screenOptions={{       
      
      tabBarActiveTintColor: "#ffffff",
      tabBarInactiveTintColor: "#ccd4e3",
      tabBarIndicatorStyle: {
        backgroundColor: "#ffffff"
      },
      tabBarLabelStyle: {
        fontSize: 10, 
      },
      tabBarStyle: { 
        backgroundColor: "#293A59"
      },
      
    }}
    
    >
      <Tab2.Screen name="Políticas de Calidad" component={Politica_Calidad} />      
      <Tab2.Screen name="Mapa de Procesos" component={Mapa_Procesos} />
      <Tab2.Screen name="Mapa Estratégico" component={Mapa_Estrategico} />      
      <Tab2.Screen name="Infraestructura" component={Infraestructura} />
    </Tab2.Navigator>
  );
}

