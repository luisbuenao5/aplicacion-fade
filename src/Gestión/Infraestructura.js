import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  ScrollView,
  Modal,
  Dimensions,
} from "react-native";
import { Card } from "react-native-paper";
import config from "../../config";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{ url: config.API_URL_IMG + "/infraestructura.jpeg" }];

class Infraestructura extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }

  _showZoom = () => this.setState({ visible: true });
  _hideZoom = () => this.setState({ visible: false });

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={require("../../assets/images/fade-transparente.png")}
          style={{ flex: 1, flexDirection: "column" }}
        >
          <ScrollView>
            <View style={styles.infraestructura}>
              <Text style={styles.titulo}>Infraestructura</Text>
            </View>

            <View style={{ alignSelf: "center", paddingTop: 120 }}>
              <Card
                style={{ height: 250, width: 360 }}
                onPress={() => this._showZoom()}
              >
                <Card.Cover
                  style={{ height: 250, width: 360, alignSelf: "center" }}
                  source={{ uri: config.API_URL_IMG + "/infraestructura.jpeg" }}
                />
              </Card>
            </View>

            <Modal
              visible={this.state.visible}
              onRequestClose={this._hideZoom}
              supportedOrientations={["portrait", "landscape"]}
              animationType={"fade"}
            >
              <ImageViewer imageUrls={images} saveToLocalByLongPress={false} />
            </Modal>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingTop: 0,
    flexDirection: "column",
  },
  infraestructura: {
    marginTop: 5,
    alignSelf: "center",
  },

  titulo: {
    paddingTop: 20,
    fontWeight: "bold",
    fontSize: 30,
    color: "#344a72",
  },
});

export default Infraestructura;
