import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  ScrollView,
  Modal,
} from "react-native";
import { Card } from "react-native-paper";
import config from "../../config";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{ url: config.API_URL_IMG + "/mapa-procesos.PNG" }];

class Mapa_Procesos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }

  _showZoom = () => this.setState({ visible: true });
  _hideZoom = () => this.setState({ visible: false });

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={require("../../assets/images/fade-transparente.png")}
          style={{ flex: 1, flexDirection: "column" }}
        >
          <ScrollView>
            <View style={styles.mapa_procesos}>
              <Text style={styles.titulo}>Mapa de Procesos</Text>
            </View>

            <View style={{ alignSelf: "center", paddingTop: 100 }}>
              <Card
                style={{ height: 350, width: 350 }}
                onPress={() => this._showZoom()}
              >
                <Card.Cover
                  style={{ height: 300, width: 350, alignSelf: "center" }}
                  source={{ uri: config.API_URL_IMG + "/mapa-procesos.PNG" }}
                />
              </Card>
            </View>

            <Modal
              visible={this.state.visible}
              transparent={true}
              onRequestClose={this._hideZoom}
              animationType={"fade"}
            >
              <ImageViewer imageUrls={images} saveToLocalByLongPress={false} />
            </Modal>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingTop: 0,
    flexDirection: "column",
  },
  mapa_procesos: {
    flex: 1,
    marginTop: 5,
    alignSelf: "center",
  },

  titulo: {
    paddingTop: 20,
    fontWeight: "bold",
    fontSize: 30,
    color: "#344a72",
  },
});

export default Mapa_Procesos;
