import React, { Component } from "react";
import { StyleSheet, View, Text, Image, ScrollView } from "react-native";
import {
  Appbar,
  Provider as PaperProvider,
  Chip,
  Divider,
} from "react-native-paper";
import Communications from "react-native-communications";
import config from "../config";

class Acerca_de extends Component {
  render() {
    return (
      <View style={styles.container}>
        <PaperProvider>
          <Appbar.Header style={{ backgroundColor: "#172133" }}>
            <Appbar.BackAction onPress={() => this.props.navigation.goBack()} />
            <Appbar.Content title="Acerca de FADE App" />
          </Appbar.Header>

          <ScrollView>
            <View style={styles.header}>
              <Text style={styles.titulo}>FADE App</Text>
              <Text style={styles.subtitulo}>Versión 1.0.1</Text>
              <Image
                source={{ uri: config.API_URL_LOG + "/logo-fade-color.png" }}
                
                //source={require("../assets/images/logo-fade-color.png")}
                style={styles.logo}
              />
            </View>

            <View>
              <Text style={styles.text}>
                La Facultad de Administración de Empresas es una unidad
                académica de la Escuela Superior Politécnica de Chimborazo; en
                la actualidad se encuentra en la implantación de su modelo de
                gestión aplicando conceptos contemporáneos de administración y
                cumplimiento de estandartes de calidad nacionales e
                internacionales.
              </Text>
            </View>

            <View>
              <Text style={styles.text}>
                La aplicación FADE App, cumple el objetivo de acercar a la
                facultad a todas sus partes interesadas y establecer un nexo de
                comunicación alineado a las nuevas herramientas tecnológicas
                para satisfacer todos los requerimientos de información, así
                como comunicar las actividades desarrolladas, oferta académicas,
                y toda la información que se genera.{" "}
              </Text>
            </View>

            <View>
              <Text style={styles.text}>
                La aplicación FADE App ha sido desarrollado por el Centro de
                Investigación en Modelos de Gestión y Sistemas Informáticos
                CIMOGSYS dentro del desarrollo de su proyecto de investigación
                “Modelo de Gestión ALPA” y sus propuestas de innovación.{" "}
              </Text>
            </View>
          </ScrollView>
          <Text>{"\n"}</Text>

          <Divider />

          <View style={styles.footer1}>
            <Chip
              style={{ backgroundColor: "#f7faff", marginTop: 25 }}
              textStyle={{ fontSize: 15 }}
              height={34}
              width={90}
              mode="outlined"
              icon="facebook"
              selected="true"
              selectedColor="#3b5998"
              onPress={() =>
                Communications.web(
                  "https://www.facebook.com/Fade-Oficial-386278805294205"
                )
              }
            >
              FADE
            </Chip>
            <Chip
              style={{
                backgroundColor: "#e6f8fa",
                marginTop: 25,
                marginLeft: 40,
              }}
              textStyle={{ fontSize: 15 }}
              height={34}
              width={90}
              mode="outlined"
              icon="twitter"
              selected="true"
              selectedColor="#00acee"
              onPress={() =>
                Communications.web("https://twitter.com/fadeespoch?lang=es")
              }
            >
              FADE
            </Chip>
          </View>
          <View style={styles.footer2}>
            <Chip
              style={{ backgroundColor: "#f7faff", marginTop: 10 }}
              textStyle={{ fontSize: 15 }}
              height={34}
              width={120}
              mode="outlined"
              icon="facebook"
              selected="true"
              selectedColor="#3b5998"
              onPress={() =>
                Communications.web("https://www.facebook.com/centro.cimogsys/")
              }
            >
              Cimogsys
            </Chip>
            <Chip
              style={{
                backgroundColor: "#fff3f2",
                marginTop: 10,
                marginLeft: 40,
              }}
              textStyle={{ fontSize: 15 }}
              height={34}
              width={120}
              mode="outlined"
              icon="youtube"
              selected="true"
              selectedColor="#c4302b"
              onPress={() =>
                Communications.web(
                  "https://www.youtube.com/channel/UCzmXPIstvvY1zFzJI6Y_kpg"
                )
              }
            >
              Cimogsys
            </Chip>
          </View>
          <View style={styles.footer3}>
            <Chip
              style={{ backgroundColor: "#fff5f8", marginTop: 10 }}
              textStyle={{ fontSize: 15 }}
              height={34}
              width={120}
              mode="outlined"
              icon="instagram"
              selected="true"
              selectedColor="#C13584"
              onPress={() =>
                Communications.web(
                  "https://www.instagram.com/cimogsys/?hl=es-la"
                )
              }
            >
              Cimogsys
            </Chip>
          </View>
          <View>
            <Text style={styles.autor}>
                    @ESPOCH  J.L. Buenaño - EIS
            </Text>
          </View>
        </PaperProvider>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    flexDirection: "column",
  },
  header: {
    alignItems: "center",
    paddingTop: 20,
  },
  text: {
    fontSize: 18,
    fontFamily: "antic-slab",
    color: "#4d4d4d",
    textAlign: "justify",
    paddingTop: 20,
    paddingLeft: 35,
    paddingRight: 35,
  },
  autor: {
    fontSize: 9,
    fontFamily: "antic-slab",
    color: "#4d4d4d",
    textAlign: "center",
  },
  titulo: {
    fontWeight: "bold",
    fontSize: 30,
    color: "#344a72",
  },
  subtitulo: {
    fontWeight: "bold",
    fontSize: 15,
    color: "#344a72",
    paddingBottom: 10,
  },
  logo: {
    width: 100,
    height: 100,
    resizeMode: "contain",
  },
  footer: {
    alignSelf: "center",
  },
  footer1: {
    flexDirection: "row",
    alignSelf: "center",
  },
  footer2: {
    flexDirection: "row",
    alignSelf: "center",
  },
  footer3: {
    alignSelf: "center",
    paddingBottom: 30,
  },
});
export default Acerca_de;